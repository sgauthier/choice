#!/bin/bash

. color.sh
. cursor.sh
. keyboard.sh

print_statusbar() {
    {
        cursor_pos $(tput lines)
        color_positive
        erase_in_line 2
        [ "$timeout" -ge 0 ] && printf 'Timeout %d\r' "$timeout"
        [ -z "$userinput" ] || printf '/%s' "$userinput"
    } > /dev/tty
}

print_entry() {
    text="$(sed -n "$(($1+1))p" <<< "$filtered" | eval "$printfilter")"
    max="$(($(tput cols) - 3))"
    [ "${#text}" -ge "$max" ] && text="${text:0:$max}..."
    [ "$1" -eq "$entry" ] && color_negative || color_positive
    cursor_pos "$(($1+1))"
    printf '%s' "$text"
}

print_all() {
    printf '%s' "$(erase_display 2; for (( i=0; i<size; i++)); do print_entry "$i"; done)" > /dev/tty
    print_statusbar
}

update_filtered() {
    if [ -z "$1" ]; then
        filtered="$(tail "-n+$((offset+1))" <<< "$STDIN" | head "-n$(($(tput lines)-1))")"
    else
        filtered="$(grep -i "$1" <<< "$STDIN" | tail "-n+$((offset+1))" | head "-n$(($(tput lines)-1))")"
    fi
    size="$(wc -l <<< "$filtered")"
}

change_entry() {
    old="$entry"
    entry="$1"
    print_entry "$old" > /dev/tty
    print_entry "$entry" > /dev/tty
}

select_entry() {
    entry="$1"
    offset=0
    userinput=""
    update_filtered "$userinput"

    cursor_hide > /dev/tty
    print_all

    while true; do
        read_key
        timeout=-1
        kill $(jobs -p) 2>/dev/null; wait $(jobs -p) 2>/dev/null
        print_statusbar
        case $KEY in
            DOWN)
                if [ "$size" -gt 0 ]; then
                    if [ "$entry" -lt "$((size-1))" ]; then
                        change_entry $((entry+1))
                    elif [ "$size" -ge "$(($(tput lines)-1))" ]; then
                        ((offset += $(($(tput lines)-1))))
                        update_filtered "$userinput"
                        entry=0
                        print_all
                    fi
                fi
                ;;
            UP)
                if [ "$size" -gt 0 ]; then
                    if [ "$entry" -gt 0 ]; then
                        change_entry $((entry-1))
                    elif [ "$offset" -gt 0 ]; then
                        ((offset -= $(($(tput lines)-1))))
                        [ "$offset" -lt 0 ] && offset=0
                        update_filtered "$userinput"
                        entry="$((size-1))"
                        print_all
                    fi
                fi
                ;;
            ENTER)
                [ "$size" -gt 0 ] && exit ;;
            [a-zA-Z])
                userinput="$userinput$KEY"
                update_filtered "$userinput"
                entry=0
                print_all
                ;;
            BACKSPACE)
                [ -z "$userinput" ] || userinput="${userinput:0:${#userinput}-1}"
                update_filtered "$userinput"
                entry=0
                print_all
                ;;
            ESC)
                filtered=""
                exit
                ;;
        esac
    done
}

index=0
timeout=-1
defaultentry=0
printfilter='cut -d" " -f2-'
selectfilter='cut -d" " -f1'

while [ $# -gt 0 ]; do
    case "$1" in
        -t) shift; timeout="${1:-0}"; shift ;;
        -e) shift; defaultentry="$1"; shift ;;
        --print-filter) shift; printfilter="$1"; shift ;;
        --select-filter) shift; selectfilter="$1"; shift ;;
        *)
            printf 'Error: invalid option "%s"\n' "$1" >&2
            exit 1
            ;;
    esac
done

while read LINE ; do
    STDIN="$STDIN$LINE"$'\n'
    ((index++))
done

[ "$timeout" -ge 0 ] && { while true; do sleep 1; kill -USR1 $$; done; } &
trap '((timeout--)); [ "$timeout" -eq 0 ] && exit; erase_in_line 2 > /dev/tty; print_statusbar' USR1
trap 'stty echo < /dev/tty; kill $(jobs -p) 2>/dev/null; wait $(jobs -p) 2>/dev/null; (cursor_show; erase_display 2; color_positive; cursor_pos) > /dev/tty; sed -n "$((entry+1))p" <<< "$filtered" | eval "$selectfilter"' EXIT

select_entry "$defaultentry"
