# choice
A small console based menu selector, inspired by dmenu, written in bash

Reads STDIN, filters each line to separate the key and the value, then displays 
the values in an interactive menu, and prints the key of the selected value in
STDOUT.

Arguments:

`-t timeout` specifies a timeout after which the default entry is selected. 0
means no timeout. Default is 0.

`-e n` specifies the default entry, as an integer starting from 0.

`--print-filter cmd` will pipe each line read in STDIN into `cmd` before
displaying them.

`--select-filter cmd` will pipe the selected line into `cmd` before printing it
in STDOUT.

**Requires the color.sh, keyboard.sh and cursor.sh scripts (https://framagit.org/fxcarton/bash-scripts) in the PATH**
